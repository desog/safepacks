package com.example.pavel.safepack;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class GPS extends AppCompatActivity  implements OnMapReadyCallback{

    private MapView mapView;
    private GoogleMap gmap;
    private LocationManager locationManager;
    private LocationListener listener;
    private Double longitud;
    private Double latitud;
    private Gson gson;
    String UsuarioFinal;
    ArrayList<LatLng> lista;
    RequestQueue requestQueue;
    LatLng ny;
    MarkerOptions markerOptions;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);

        Bundle extras = getIntent().getExtras();

        if(extras != null){
            UsuarioFinal = extras.getString("usuario");
        }

        requestQueue  = Volley.newRequestQueue(this);

       configure_button();

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        mapView = findViewById(R.id.map_view);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

    }

    public void volver(View view){
        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;
        gmap.setIndoorEnabled(true);
        UiSettings uiSettings = gmap.getUiSettings();
        uiSettings.setIndoorLevelPickerEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setCompassEnabled(true);
        uiSettings.setZoomControlsEnabled(true);



        Log.w("GPS","GPS: entro a la actividad");

        SingleShotLocationProvider.requestSingleUpdate(this,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        Log.w("GPS", "GPS: my location is " + location.latitude + ";" + location.longitude);

                        if(location != null) {
                            ny = new LatLng(location.latitude, location.longitude);
                            markerOptions = new MarkerOptions();

                            lista = new ArrayList<>();
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                            gson = gsonBuilder.create();
                            StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://api.appery.io/rest/1/apiexpress/api/arqui/datosuser/"+UsuarioFinal+"?apiKey=560bf750-c3dc-455a-b859-9f0bf69e025c",
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            JSONArray ja = null;
                                            try {
                                                ja = new JSONArray(response);
                                                for(int i = 0; i < ja.length(); i++) {
                                                    String nombre = ja.getJSONObject(i).getString("usuario");
                                                    if(nombre.equals(UsuarioFinal)){
                                                        String loc = ja.getJSONObject(i).getString("localizacion");

                                                        loc = loc.replace("\\s", "");
                                                        String A[] = loc.split(",");
                                                        double a = Double.parseDouble(A[0]);
                                                        double b = Double.parseDouble(A[1]);
                                                        Log.w("LOCALIZACION", "Localizacion " + a+ " "+b);
                                                        markerOptions.position(new LatLng(a, b));
                                                        gmap.addMarker(markerOptions);

                                                    }
                                                }
                                                markerOptions.position(ny);
                                                gmap.addMarker(markerOptions);
                                                gmap.moveCamera(CameraUpdateFactory.newLatLng(ny));
                                                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(ny, 16);
                                                gmap.animateCamera(yourLocation);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                }
                            });
                            requestQueue.add(stringRequest);

                        }
                    }
                });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                configure_button();
                break;
            default:
                break;
        }
    }

    void configure_button() {
        // first check for permissions

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }

        // this code won't execute IF permissions are not allowed, because in the line above there is return statement.

    }

}
