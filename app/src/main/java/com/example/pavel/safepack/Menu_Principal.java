package com.example.pavel.safepack;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Menu_Principal extends AppCompatActivity {//extends AbstractActivity {
    Intent msgIntent = null;
    String UsuarioFinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__principal);

        Bundle extras = getIntent().getExtras();

        if(extras != null){
            UsuarioFinal = extras.getString("usuario");
        }


        Log.w("SEGUNDA","SEGUNDA: CREARA EL INTENTO");
        msgIntent = new Intent(Menu_Principal.this, Segunda.class);
        Log.w("SEGUNDA","SEGUNDA: INICIARA EL SERVICIO");
        startService(msgIntent);
        Log.w("SEGUNDA","SEGUNDA: LO INICIO");
    }

    public void backpackmonitor(View view){

        Intent intent = new Intent(Menu_Principal.this, Monitor.class);
        intent.putExtra("usuario", UsuarioFinal);
        startActivity(intent);
    }

    public void gps(View view){
        Intent intent = new Intent(this, GPS.class);
        Log.w("GPS","GPS: entrara a la actividad");
        intent.putExtra("usuario", UsuarioFinal);
        startActivity(intent);
    }

    public void alarma(View view){
        //Intent msgIntent = new Intent(Menu_Principal.this, Segunda.class);
        Log.w("SEGUNDA","SEGUNDA: DETENDRA EL SERVICIO");
        stopService(msgIntent);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Menu_Principal.this, alarma.class);
                startActivity(intent);
            }
        }, 2000);


    }

    public void ayuda(View view){
        Intent intent = new Intent(this, Ayuda.class);
        intent.putExtra("usuario", UsuarioFinal);
        startActivity(intent);
    }

    public void logout(View view){
        finish();
    }
}
