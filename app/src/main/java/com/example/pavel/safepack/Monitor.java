package com.example.pavel.safepack;

import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class Monitor extends AppCompatActivity {

    private Gson gson;

    ArrayList<PromedioDia> Lista_Diario;
    BarChart barChart;
    Spinner combo;
    String url;
    String Descripcion;
    String UsuarioFinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor);

        Bundle extras = getIntent().getExtras();

        if(extras != null){
            UsuarioFinal = extras.getString("usuario");
            Log.w("USUARIO ","Usuario "+UsuarioFinal);
        }


        barChart = findViewById(R.id.barchart);
        combo = findViewById(R.id.cambio);
        combo.setPrompt("Tipo de Informacion");

        combo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                String seleccionado = combo.getSelectedItem().toString();
                Lista_Diario = new ArrayList<>();
                if(position == 0){
                    url = "https://api.appery.io/rest/1/apiexpress/api/arqui/avgpeso?apiKey=560bf750-c3dc-455a-b859-9f0bf69e025c&usuario="+UsuarioFinal;
                    Descripcion = "Pesos Diarios Promedios";
                    ObtenerDatos("promedio");
                }else if(position == 1){
                    url = "https://api.appery.io/rest/1/apiexpress/api/arqui/avginclinacionfecha?apiKey=560bf750-c3dc-455a-b859-9f0bf69e025c&usuario="+UsuarioFinal;
                    Descripcion = "Inclinación Diarios Promedios";
                    ObtenerDatos("inclinacion");
                }else if(position == 2){
                    Log.w("POSICION ","POSICION "+position);
                    url = "https://api.appery.io/rest/1/apiexpress/api/arqui/countalarma?apiKey=560bf750-c3dc-455a-b859-9f0bf69e025c&usuario="+UsuarioFinal;
                    Descripcion = "Cantidad de Veces Activa Alarma Diariamente";
                    ObtenerDatos("count");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });




    }

    public void ObtenerDatos(final String title1){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONArray ja = null;
                        try {
                            ja = new JSONArray(response);
                            for(int i = 0; i < ja.length(); i++) {
                                String fecha = ja.getJSONObject(i).getString("fecha");
                                float valor = Float.parseFloat(ja.getJSONObject(i).getString(title1));
                                Log.w("POS ","VALOR "+valor);
                                Lista_Diario.add(new PromedioDia(valor, fecha));

                            }
                            setDatos(Lista_Diario,Descripcion);
                            for(PromedioDia i : Lista_Diario){
                                Log.w("LOGARITMO","Grado: "+i.getValor()+" Fecha: "+i.getFecha());
                            }
                        } catch (JSONException e) {
                            Log.w("LOGARITMO","LOGARITMO ERROR");
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue.add(stringRequest);
    }

    public void setDatos(ArrayList<PromedioDia> list, String descripcion){
        ArrayList<BarEntry> bargroup1 = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();

        int j = 0;

        for(PromedioDia i : list){
            bargroup1.add(new BarEntry(i.getValor(), j));
            labels.add(i.getFecha()+"");
            j++;
        }

        BarDataSet bardataset = new BarDataSet(bargroup1, "Promedios");
        bardataset.setColors(ColorTemplate.PASTEL_COLORS);
        BarData data = new BarData(labels, bardataset);
        barChart.setDescription(descripcion);
        barChart.setData(data);
    }




}
