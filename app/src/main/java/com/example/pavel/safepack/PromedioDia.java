package com.example.pavel.safepack;

import java.util.Date;

public class PromedioDia {
    private String fecha;
    private float valor;
    private int valor1;

    public PromedioDia(float valor, String fecha){
        this.valor = valor;
        this.fecha = fecha;
    }

    public PromedioDia(int valor1, String fecha){
        this.valor1 = valor1;
        this.fecha = fecha;
    }

    public float getValor(){
        return valor;
    }

    public float getValor1(){
        return valor1;
    }


    public String getFecha(){
        return fecha;
    }
}
