package com.example.pavel.safepack;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class Segunda extends Service {

    final int handlerState = 0;                        //used to identify handler message
    Handler bluetoothIn;
    private BluetoothAdapter btAdapter = null;

    private ConnectingThread mConnectingThread;
    private ConnectedThread mConnectedThread;

    private boolean stopThread;
    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    // String for MAC address
    private static final String MAC_ADDRESS = "98:D3:32:31:0F:03";

    private StringBuilder recDataString = new StringBuilder();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.w("SEGUNDA", "SEGUNDA: SERVICE CREATED");
        stopThread = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.w("SEGUNDA", "SEGUNDA: SERVICE STARTED");
        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {
                Log.w("SEGUNDA", "SEGUNDA: handleMessage");
                String dataInPrint = "";
                if (msg.what == handlerState) {                                     //if message is what we want
                    String readMessage = (String) msg.obj;                                                                // msg.arg1 = bytes from connect thread
                    recDataString.append(readMessage);
                    int endOfLineIndex = recDataString.indexOf("#");                    // determine the end-of-line
                    if (endOfLineIndex > 0) {                                           // make sure there data before ~
                        dataInPrint = recDataString.substring(0, endOfLineIndex);
                        Log.w("SEGUNDA","SEGUNDA: *******************************************"+dataInPrint);// extract string
                        enviarDatos(dataInPrint);
                        recDataString.delete(0, recDataString.length());                    //clear all string data
                    }
                }
            }
        };

        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();
        return super.onStartCommand(intent, flags, startId);
    }

    public void enviarDatos(String datos){
        int aux = datos.indexOf(";");
        String ace = datos.substring(0,aux);
        Log.w("SEGUNDA","SEGUNDA: acelerometro: "+ ace);
        String peso = datos.substring(aux + 1);
        Log.w("SEGUNDA","SEGUNDA: peso: "+ peso);

        Calendar cal = Calendar.getInstance();
        Date date=cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault()); //new SimpleDateFormat("HH:mm");
        String formattedDate=dateFormat.format(date);
        int aux2 = formattedDate.indexOf(" ");
        String fecha = formattedDate.substring(0,aux2);
        Log.w("SEGUNDA","SEGUNDA: fecha: "+ fecha);
        String hora = formattedDate.substring(aux2 + 1);
        Log.w("SEGUNDA","SEGUNDA: hora: "+ hora);

        final String data = "{"+
                "\"peso\": 12," +
                "\"inclinacion\": 24," +
                "\"alarma\": 0," +
                "\"localizacion\": \"14.586817,-90.554362\"," +
                "\"fecha\": \"17-11-2018\"," +
                "\"hora\": \"21:41\"," +
                "\"usuario\": \"pavel\"" +
                "}";

        String URL = "https://api.appery.io/rest/1/apiexpress/api/arqui/insertar?apiKey=560bf750-c3dc-455a-b859-9f0bf69e025c"
                + "&peso=" + peso
                + "&inclinacion=" + ace
                + "&alarma=" + "1"
                + "&localizacion=" + "14.6058133,-90.5799533"
                + "&fecha=" + fecha
                + "&hora=" + hora
                + "&usuario=" + "daniel";
        Log.w("SEGUNDA","SEGUNDA: url: " + URL);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject objres = new JSONObject(response);
                    Log.w("SEGUNDA","SEGUNDA: datos enviados");
                    //Toast.makeText(getApplicationContext(),"Usuario registrado",Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    //Toast.makeText(getApplicationContext(),"Server Error",Toast.LENGTH_LONG).show();
                    Log.w("SEGUNDA","SEGUNDA: datos NO enviados");
                }
            }
        },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                })
        {
            @Override
            public String getBodyContentType(){
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try{
                    return data == null ? null : data.getBytes("utf-8");
                }
                catch(UnsupportedEncodingException uee){
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        bluetoothIn.removeCallbacksAndMessages(null);
        stopThread = true;
        if (mConnectedThread != null) {
            mConnectedThread.closeStreams();
        }
        if (mConnectingThread != null) {
            mConnectingThread.closeSocket();
        }
        Log.w("SEGUNDA", "SEGUNDA: onDestroy");
    }

    //@Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
    private void checkBTState() {

        if (btAdapter == null) {
            Log.w("SEGUNDA", "SEGUNDA: BLUETOOTH NOT SUPPORTED BY DEVICE, STOPPING SERVICE");
            stopSelf();
        } else {
            if (btAdapter.isEnabled()) {
                Log.w("SEGUNDA", "SEGUNDA: BT ENABLED! BT ADDRESS : " + btAdapter.getAddress() + " , BT NAME : " + btAdapter.getName());
                try {
                    BluetoothDevice device = btAdapter.getRemoteDevice(MAC_ADDRESS);
                    Log.w("SEGUNDA", "SEGUNDA: ATTEMPTING TO CONNECT TO REMOTE DEVICE : " + MAC_ADDRESS);
                    mConnectingThread = new ConnectingThread(device);
                    mConnectingThread.start();
                } catch (IllegalArgumentException e) {
                    Log.w("SEGUNDA", "SEGUNDA: PROBLEM WITH MAC ADDRESS : " + e.toString());
                    Log.w("SEGUNDA", "SEGUNDA: ILLEGAL MAC ADDRESS, STOPPING SERVICE");
                    stopSelf();
                }
            } else {
                Log.w("SEGUNDA", "SEGUNDA: BLUETOOTH NOT ON, STOPPING SERVICE");
                stopSelf();
            }
        }
    }

    // New Class for Connecting Thread
    private class ConnectingThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectingThread(BluetoothDevice device) {
            Log.w("SEGUNDA", "SEGUNDA: IN CONNECTING THREAD");
            mmDevice = device;
            BluetoothSocket temp = null;
            Log.w("SEGUNDA", "SEGUNDA: MAC ADDRESS : " + MAC_ADDRESS);
            Log.w("SEGUNDA", "SEGUNDA: BT UUID : " + BTMODULEUUID);
            try {
                temp = mmDevice.createRfcommSocketToServiceRecord(BTMODULEUUID);
                Log.w("SEGUNDA", "SEGUNDA: SOCKET CREATED : " + temp.toString());
            } catch (IOException e) {
                Log.w("SEGUNDA", "SEGUNDA: SOCKET CREATION FAILED :" + e.toString());
                Log.w("SEGUNDA", "SEGUNDA: SOCKET CREATION FAILED, STOPPING SERVICE");
                stopSelf();
            }
            mmSocket = temp;
        }

        @Override
        public void run() {
            super.run();
            Log.w("SEGUNDA", "SEGUNDA: IN CONNECTING THREAD RUN");
            // Establish the Bluetooth socket connection.
            // Cancelling discovery as it may slow down connection
            btAdapter.cancelDiscovery();
            try {
                mmSocket.connect();
                Log.w("SEGUNDA", "SEGUNDA: BT SOCKET CONNECTED");
                mConnectedThread = new ConnectedThread(mmSocket);
                mConnectedThread.start();
                Log.w("SEGUNDA", "SEGUNDA: CONNECTED THREAD STARTED");
                //I send a character when resuming.beginning transmission to check device is connected
                //If it is not an exception will be thrown in the write method and finish() will be called
                //mConnectedThread.write("prueba");
            } catch (IOException e) {
                try {
                    Log.w("SEGUNDA", "SEGUNDA: SOCKET CONNECTION FAILED : " + e.toString());
                    Log.w("SEGUNDA", "SEGUNDA: SOCKET CONNECTION FAILED, STOPPING SERVICE");
                    mmSocket.close();
                    stopSelf();
                } catch (IOException e2) {
                    Log.w("SEGUNDA", "SEGUNDA: SOCKET CLOSING FAILED :" + e2.toString());
                    Log.w("SEGUNDA", "SEGUNDA: SOCKET CLOSING FAILED, STOPPING SERVICE");
                    stopSelf();
                    //insert code to deal with this
                }
            } catch (IllegalStateException e) {
                Log.w("SEGUNDA", "SEGUNDA: CONNECTED THREAD START FAILED : " + e.toString());
                Log.w("SEGUNDA", "SEGUNDA: CONNECTED THREAD START FAILED, STOPPING SERVICE");
                stopSelf();
            }
        }

        public void closeSocket() {
            try {
                //Don't leave Bluetooth sockets open when leaving activity
                mmSocket.close();
            } catch (IOException e2) {
                //insert code to deal with this
                Log.w("SEGUNDA", "SEGUNDA: " + e2.toString());
                Log.w("SEGUNDA", "SEGUNDA: SOCKET CLOSING FAILED, STOPPING SERVICE");
                stopSelf();
            }
        }
    }

    // New Class for Connected Thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            Log.w("SEGUNDA", "SEGUNDA: IN CONNECTED THREAD");
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.w("SEGUNDA", "SEGUNDA: " + e.toString());
                Log.w("SEGUNDA", "SEGUNDA: UNABLE TO READ/WRITE, STOPPING SERVICE");
                stopSelf();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            Log.w("SEGUNDA", "SEGUNDA: IN CONNECTED THREAD RUN");
            byte[] buffer = new byte[256];
            int bytes;

            // Keep looping to listen for received messages
            while (true && !stopThread) {
                try {
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    Log.w("SEGUNDA", "SEGUNDA: CONNECTED THREAD Mensaje final " + readMessage);
                    // Send the obtained bytes to the UI Activity via handler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    Log.w("SEGUNDA", "SEGUNDA: " + e.toString());
                    Log.w("SEGUNDA", "SEGUNDA: UNABLE TO READ/WRITE, STOPPING SERVICE");
                    stopSelf();
                    break;
                }
            }
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Log.w("SEGUNDA", "UNABLE TO READ/WRITE " + e.toString());
                Log.w("SEGUNDA", "UNABLE TO READ/WRITE, STOPPING SERVICE");
                stopSelf();
            }
        }

        public void closeStreams() {
            try {
                //Don't leave Bluetooth sockets open when leaving activity
                mmInStream.close();
                mmOutStream.close();
            } catch (IOException e2) {
                //insert code to deal with this
                Log.w("SEGUNDA", e2.toString());
                Log.w("SEGUNDA", "STREAM CLOSING FAILED, STOPPING SERVICE");
                stopSelf();
            }
        }
    }
}
