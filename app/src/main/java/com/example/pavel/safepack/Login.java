package com.example.pavel.safepack;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {

    private Button botonRegistro;
    private EditText user;
    private EditText password;

    private Gson gson;

    String nombre = "";
    String pass = "";
    String correo = "";
    String UsuarioFinal;

    String nombre_usuario = "";
    String contraseña = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //irMenu();
    }

    public void irRegistro(View view){
        Intent intent = new Intent(this, Registro.class);
        startActivity(intent);
    }

    public void IniciarSesion(View view){

        user = findViewById(R.id.usuario);
        password = findViewById(R.id.password);

        nombre_usuario = user.getText().toString();
        contraseña =  password.getText().toString();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String url = "https://api.appery.io/rest/1/apiexpress/api/arqui/"+nombre_usuario+"?apiKey=fc618a1e-8b88-4149-8283-5c24adad4a07";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        JSONObject jo = null;
                        try {
                            jo = new JSONObject(response);

                            nombre = jo.getString("usuario");
                            pass = jo.getString("password");
                            correo = jo.getString("correo");
                            System.out.println(correo);

                            System.out.println("USUARIOS: "+ nombre_usuario+" "+nombre);
                            System.out.println("CONTRAS: "+ contraseña+" "+pass);
                            if(nombre.equals(nombre_usuario) && pass.equals(contraseña)){
                                UsuarioFinal = nombre;
                                irMenu();
                            }else{
                                Context context = getApplicationContext();
                                CharSequence text = "Datos Incorrectos";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        requestQueue.add(stringRequest);
    }

    public void irMenu(){
        Intent intent = new Intent(this, Menu_Principal.class);
        intent.putExtra("usuario", UsuarioFinal);
        startActivity(intent);
    }


}
