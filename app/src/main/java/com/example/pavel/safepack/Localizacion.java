package com.example.pavel.safepack;

public class Localizacion {
    private float Longitud;
    private float Latitud;

    public Localizacion(float Longitud, float Latitud){
        this.Longitud = Longitud;
        this.Latitud = Latitud;
    }

    public float getLongitud(){
        return Longitud;
    }

    public float getLatitud() {
        return Latitud;
    }
}
