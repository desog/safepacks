package com.example.pavel.safepack;

import java.sql.Time;
import java.util.Date;

public class Inclinacion {
    private int Grados;
    private Date Fecha;

    public Inclinacion(int Grados, Date Fecha){
        this.Grados = Grados;
        this.Fecha = Fecha;
    }

    public Date getFecha() {
        return Fecha;
    }

    public int getGrados() {
        return Grados;
    }

    public void setGrados(int grados) {
        Grados = grados;
    }
}

