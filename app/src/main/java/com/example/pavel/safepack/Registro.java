package com.example.pavel.safepack;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Registro extends AppCompatActivity {
    private EditText user;
    private EditText password;
    private EditText correo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
    }

    public void Volver(View view){
        finish();
    }

    public void Registrar(View view){
        user = findViewById(R.id.usuario);
        password = findViewById(R.id.password);
        correo = findViewById(R.id.correo);

        final String data = "{"+
                "\"correo\":\"" + correo.getText().toString() + "\"," +
                "\"password\":\"" + password.getText().toString() + "\"," +
                "\"usuario\":\"" + user.getText().toString() + "\"" +
                "}";

        String URL = "https://api.appery.io/rest/1/apiexpress/api/arqui?apiKey=fc618a1e-8b88-4149-8283-5c24adad4a07";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject objres = new JSONObject(response);
                            //Toast.makeText(getApplicationContext(),objres.toString(),Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(),"Usuario registrado",Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),"Server Error",Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                })
        {
            @Override
            public String getBodyContentType(){
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError{
                try{
                    return data == null ? null : data.getBytes("utf-8");
                }
                catch(UnsupportedEncodingException uee){
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
    }
}
